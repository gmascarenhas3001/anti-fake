const express = require('express');
const app = express();
const bodyParser = require("body-parser");
const path = require("path");
const cors = require("cors");
const PORT = 3030;
const mysql = require('mysql');
const multer = require('multer');

var upload = multer({ dest: 'src/assets/img' });

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors({ origin: "*" }));
app.use(express.urlencoded({ extended: false }));


const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Ayrton2017;',
    database: 'antifake',
    multipleStatement: true
});

connection.connect(function(error) {
    if (!error) {
        console.log("CONNECTED");
    } else {
        console.log("ERROR not connected");
    }
});

var fileNameImage;

///Upload images with multer/////////////////////////////////////////////////////////////////////7

const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, 'src/assets/img')
    },
    filename: (req, file, callBack) => {
        callBack(null, `fileUpload${file.originalname}`)
    }
});

var upload = multer({ storage: storage });
// let upload = multer({dest: "uploads/"});



app.post('/api/articles/photo', upload.single('file'), (req, res, next) => {
    const file = req.file;
    fileNameImage = file.filename;
    console.log("SERVER=>", fileNameImage);
    if (!file) {
        const error = new Error('Error!!!!');
        error.httpStatusCode = 400;
        return next(error);
    }
    res.send(file);

});





//Get all articles/////////////////////////////////////////////////////////////////////////////////

app.get('/api/articles', function(req, res) {
    connection.query("SELECT * FROM articles", (errors, rows, filelds) => {
        if (!errors) {
            res.send(rows);
        } else {
            console.log(errors);
        }
    });
});

//Get a articles////////////////////////////////////////////////////////////////////

app.get('/api/articles/:id', function(req, res) {
    connection.query('SELECT * FROM articles WHERE id= ?', [req.params.id], (errors, rows, fields) => {
        if (!errors) {
            res.send(rows[0]);
        } else {
            console.log(errors);
        }
    });
});

//Delete a articles////////////////////////////////////////////////////////////////////

app.delete('/api/articles/delete/:id', function(req, res) {
    connection.query('DELETE FROM articles WHERE id= ?', [req.params.id], (errors, rows, filelds) => {
        if (!errors) {
            res.send(rows);
        } else {
            console.log(errors);
        }
    });
});

//Insert a articles////////////////////////////////////////////////////////////////////////

app.post('/api/articles', function(req, res) {
    console.log("Insert", fileNameImage);
    var newDate = Date(Date.now());
    var dateText = newDate.toString();
    var photo = fileNameImage;
    let sql = "INSERT INTO articles (title, photo, text, date) VALUES ('" + req.body.title + "', '" + photo + "',  '" + req.body.text + "', '" + dateText + "')";
    connection.query(sql, (errors, resultat) => {
        if (!errors) {
            res.send(req.body);
            console.log(resultat.insertId);
        } else {
            console.log(errors);
        }
    });
});

//Update a articles////////////////////////////////////////////////////////////////////////

app.put('/api/articles/update/:id', function(req, res) {

    const userId = req.params.id;
    var newDate = Date(Date.now());
    var dateText = newDate.toString();
    var sql = "UPDATE articles SET title ='" + req.body.title + "', photo ='" + req.body.photo + "', text ='" + req.body.text + "', date ='" + dateText + "'  WHERE id ='" + userId + "'";

    connection.query(sql, (errors, rows, fields) => {
        if (!errors) {
            res.send(req.body);

        } else {
            console.log(errors);
        }
    });
});


//Get all comments by article////////////////////////////////////////////

app.get('/api/articles/:id/commentaires', function(req, res) {
    connection.query('SELECT * FROM commentaires WHERE article_id= ?', [req.params.id], (errors, rows, fields) => {
        if (!errors) {
            res.send(rows);
        } else {
            console.log(errors);
        }
    });
});


//Add comment /////////////////////////////////////////////////////////////////////////////

app.post('/api/articles/:id/commentaires', function(req, res) {

    const userId = req.params.id;
    let sql = "INSERT INTO commentaires (commentaires, article_id) VALUES ('" + req.body.commentaires + "', '" + userId + "')";
    connection.query(sql, (errors, rows, field) => {
        if (!errors) {
            res.send(req.body);


        } else {
            console.log(errors);
        }
    });

});

//Photo uploads///////////////////////////////////////////////////////////////////////////7

/* app.post('/api/articles/photo', function(req, res) {

    const userId = 13;
    console.log("Server.js");

    let sql = "INSERT INTO photo (path, article_id) VALUES ('" + req.body.path + "', '" + userId + "')";
    connection.query(sql, (errors, rows, field) => {
        if (!errors) {
            res.send(req.body);
            console.log("OKOKOK");

        } else {
            console.log(errors);
        }
    });

}); */





app.listen(PORT, function() {
    console.log(`Example app listening on port ${PORT}!`)
});

module.exports = app;