import { Component, OnInit } from '@angular/core';
import { Article } from '../article';
import { ArticleService } from '../article.service';
import { Location } from '@angular/common';
import {NgForm} from '@angular/forms';
import { Photo } from '../photo';

import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  articles: Article[];
  selectedArticle: Article;
  photo: Photo;
  images;
  selectedFile: File = null;
  imageUrl = "/assets/img/fileUploadantifakebrasil2.jpg";

  httpOptions = {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

  private heroesUrl = 'http://localhost:3030/api/articles';  // URL to web api

  

  constructor(private articleService: ArticleService,
              private location: Location,
              private http: HttpClient) { }

  ngOnInit() {
    this.getArticles();
  }


  getArticles(): void {
    this.articleService.getArticles()
        .subscribe(articles => this.articles = articles);
  }

  goBack(): void {
    this.location.back();
  }


  refresh(): void {
    window.location.reload();
  }

  delete(article: Article): void {
    this.articles = this.articles.filter(h => h !== article);
    this.articleService.deleteArticle(article).subscribe();
  }

  onSubmit(f: NgForm) {

    this.onSubmitPhoto();
    let title = f.value.title;
    let text= f.value.text;

    if (!title) { return; }
    this.articleService.addArticle({ title, text} as Article)
      .subscribe(article => {
        this.articles.push(article);
       this.refresh();
      });
    console.log(f.valid);  // false
  }

  
  selectImage(event): void{
    var file = event.target.files[0];
    this.images = file;
  } 

  onSubmitPhoto(){
    const formData = new FormData();
    formData.append('file', this.images);
    console.log("FormData", this.images);

    this.http.post<any>('http://localhost:3030/api/articles/photo', formData)
    .subscribe((res) => console.log(res),
                (err) => console.log(err));
  }



}//Class end /////////////////////////////////
