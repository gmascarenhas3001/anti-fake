import { Injectable } from '@angular/core';
import { Article } from './article';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Commentaires } from './commentaires';
import { Photo } from './photo';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  httpOptions = {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

  private heroesUrl = 'http://localhost:3030/api/articles';  // URL to web api

  constructor(private http: HttpClient) { }

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(this.heroesUrl);
  }

  getArticle(id: number): Observable<Article> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get<Article>(url);
  }

  getArticleComments(id: number): Observable<Commentaires> {
    const url = `${this.heroesUrl}/${id}/commentaires`;
    return this.http.get<Commentaires>(url);
  }

  

 
  addComment (commentaire: Commentaires): Observable<any> {  
    let url = `${this.heroesUrl}/${commentaire.article_id}/commentaires`;
    return this.http.post(url, commentaire);
  }

   /** PUT: update the article on the server */
   updateArticle (article: Article): Observable<any> {
    let url = `${this.heroesUrl}/update/${article.id}`
   return this.http.put(url, article, this.httpOptions);
    }


    /** POST: add a new article to the server */
    addArticle (article: Article): Observable<Article> {
      return this.http.post<Article>(this.heroesUrl, article, this.httpOptions);
    }

    /** DELETE: delete the hero from the server */
    deleteArticle (article: Article | number): Observable<Article> {
      const id = typeof article === 'number' ? article : article.id;
      const url = `${this.heroesUrl}/delete/${id}`;

      return this.http.delete<Article>(url, this.httpOptions);
}


    
}//Class end////////////////////////////////////////////////////7
