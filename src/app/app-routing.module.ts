import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticlesComponent } from './articles/articles.component';
import { AccueilComponent } from './accueil/accueil.component';
import { ArticleDetailComponent }  from './article-detail/article-detail.component';




const routes: Routes = [
  { path: '', redirectTo: '/accueil', pathMatch: 'full' },
  { path: 'accueil', component: AccueilComponent },
  { path: 'articles/:id', component: ArticleDetailComponent },
  { path: 'articles', component: ArticlesComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
