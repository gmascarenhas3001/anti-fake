import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../article';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {NgForm} from '@angular/forms';
import { Commentaires } from '../commentaires';

import { ArticleService }  from '../article.service';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {

  commentaires: Commentaires;  
  article: Article;

  constructor( private route: ActivatedRoute,
               private articleService: ArticleService,
               private location: Location) { }

  ngOnInit() {
    this.getArticle();
    this.getArticleComment();
    
  }

  getArticle(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.articleService.getArticle(id)
      .subscribe(article => this.article = article);
  }

  getArticleComment(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.articleService.getArticleComments(id)
      .subscribe(commentaire => this.commentaires = commentaire);
  }


  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.articleService.updateArticle(this.article)
      .subscribe(() => this.goBack());
  }

  refresh(): void {
    window.location.reload();
  }

  onSubmit(f: NgForm) {
   
    let comment = new Commentaires();
    comment.commentaires = f.value.commentaires;
    comment.article_id = this.article.id;
    console.log("aticleId",comment.article_id);

    this.articleService.addComment(comment)
    .subscribe(comt => {this.commentaires = comt;
                        this.refresh();
                        
    });

    console.log(f.valid);  // false
  }


}
