export class Article {
    id: number;
    title: string;
    photo: string;
    text: string;
    date: string;
}